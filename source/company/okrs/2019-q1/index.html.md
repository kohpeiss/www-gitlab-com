---
layout: markdown_page
title: "2019 Q1 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. More leads. Successful customers (standard implementation path, customer success based on their goals, DevOps maturity), Scalable marketing (Pipe-to-spend for all, double down on what works, more experiments with things like demo's, call us, and ebooks)

* Alliances: grow leads, customers, and scalable marketing
    * 3 public references for each major public/private cloud
    * Increased customer purchase and growth through marketplaces
    * Joint webinars with primary partners - 200+ leads per webinar

### CEO: Popular next generation product. Triple secure value (3 stages, multiple teams). Grown use of stages (SMAU). iPhone app on iPad.

* Product:
    * Increase product breadth. 25% (6 of 22) "new in 2019" categories at `minimal` maturity, at least one MVC for one new role (e.g. Designers), an impactful demo of developing an iPhone app on GitLab.com.
    * Increase product depth for existing categories. 50% (11 of 22) "new in 2018" categories at `complete` maturity. 
    * Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 6 reference customers using all stages concurrently.
* Alliances: write whitepaper and evaluate qualifying for security competancy with major clouds 

### CEO: Great team. Employed brand (known for all remote, great communication of total compensation, 10 videos per manager and up), Effective hiring (Faster apply to hire), ELO score per interviewer), Decision making effectiveness (kpis from original source and red/green, training for director group)

